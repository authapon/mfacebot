# Facebook Messenger Bot Library for Golang

**This library is just my toy library.**

**Used it with your favour.**

```go
package main

import (
    "fmt"
    Bot "gitlab.com/authapon/mfacebot"
)

var bot *Bot.Bot

func myidget(data map[string] interface{}) {
    dataSent := map[string] interface {} {
        "recipient": data["sender"].(string),
        "text": "Your ID is "+data["sender"].(string),
    }
    bot.Send(dataSent)
}

func appidget(data map[string] interface{}) {
    dataSent := map[string] interface {} {
        "recipient": data["sender"].(string),
        "text": "App ID is "+data["recipient"].(string),
    }
    bot.Send(dataSent)
}

func upCommand(data map[string] interface{}) {
    dataSent := map[string] interface{} {
        "recipient": data["sender"].(string),
        "title": "test",
        "subtitle": "by tester",
        "image_url": "https://example.com/picture.jpg",
        "item_url": "https://example.com/",
    }
    bot.Send(dataSent)
}

func elseCom(data map[string] interface{}) {
    dataSent := map[string] interface {} {
        "recipient": data["sender"].(string),
        "text": "Sorry!!! I don't understand.",
    }
    bot.Send(dataSent)

}

func web(data map[string] interface{}) {
    fmt.Printf("%s\n\n", data)
}

func main() {
    bot = Bot.New()
    bot.Host = ":5001"
    bot.Token = "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"
    bot.Verify = "VerifyText"
    bot.Static = "./static"
    bot.Command("myid", myidget)
    bot.Command("appid", appidget)
    bot.Command("up", upCommand)
    bot.CommandElse(elseCom)
    bot.Route("/web", web)
    bot.Start()

    select {}
}
```
