package mfacebot

import (
	"encoding/json"
	"errors"
	"fmt"
	"net/http"
	"strings"
	"time"
)

type workfunc func(map[string]interface{})

type Bot struct {
	Token       string
	Verify      string
	Host        string
	Static      string
	route       map[string]workfunc
	command     map[string]workfunc
	commandElse workfunc
}

var (
	api    = "https://graph.facebook.com/v2.7/me/messages?access_token="
	apiSub = "https://graph.facebook.com/v2.7/me/subscribed_apps?access_token="
)

func New() *Bot {
	bot := new(Bot)
	bot.Token = ""
	bot.Verify = ""
	bot.Host = ":4500"
	bot.Static = "./static"
	bot.route = make(map[string]workfunc)
	bot.command = make(map[string]workfunc)
	return bot
}

func readJson(r *http.Request) (interface{}, error) {
	data := make([]byte, 50000)
	nread, _ := r.Body.Read(data)
	r.Body.Close()
	body := string(data[:nread])
	var jSonData interface{}
	err := json.Unmarshal([]byte(body), &jSonData)
	if err != nil {
		return jSonData, err
	}
	return jSonData, nil
}

func queryExtract(q string) map[string]string {
	data := make(map[string]string)
	queryData := strings.Split(q, "&")
	for _, v := range queryData {
		queryEx := strings.Split(v, "=")
		if len(queryEx) != 2 {
			continue
		}
		data[queryEx[0]] = queryEx[1]
	}
	return data
}

func PostJson(url string, data string) (string, error) {
	client := &http.Client{}
	req, _ := http.NewRequest("POST", url, strings.NewReader(data))
	req.Header.Set("Content-Type", "application/json")
	resp, err := client.Do(req)

	if err == nil {
		dataBackByte := make([]byte, 50000)
		count, err := resp.Body.Read(dataBackByte)
		if err != nil {
			return "", err
		} else {
			dataBack := string(dataBackByte[:count])
			resp.Body.Close()
			return dataBack, nil
		}
	} else {
		return "", err
	}
}

func (b *Bot) handler(w http.ResponseWriter, r *http.Request) {

	if r.URL.Path == "/" && r.Method == "GET" {
		queryData := queryExtract(r.URL.RawQuery)

		if queryData["hub.verify_token"] == b.Verify {
			fmt.Fprintf(w, "%s", queryData["hub.challenge"])
		} else {
			fmt.Fprintf(w, "Error, wrong validation token")
		}
	} else if r.URL.Path == "/" && r.Method == "POST" {
		jSonData, err := readJson(r)
		if err != nil {
			return
		}

		object, ok := jSonData.(map[string]interface{})["object"].(string)
		if !ok {
			return
		}

		if object != "page" {
			return
		}

		entry, ok := jSonData.(map[string]interface{})["entry"].([]interface{})
		if !ok {
			return
		}

		for _, v := range entry {
			messaging, ok := v.(map[string]interface{})["messaging"].([]interface{})
			if !ok {
				continue
			}

			for _, fbtext := range messaging {
				message, ok := fbtext.(map[string]interface{})["message"].(map[string]interface{})
				if !ok {
					continue
				}
				sender := fbtext.(map[string]interface{})["sender"].(map[string]interface{})["id"].(string)
				recipient := fbtext.(map[string]interface{})["recipient"].(map[string]interface{})["id"].(string)

				text, ok := message["text"].(string)
				if ok {
					text2 := strings.TrimSpace(text)

					dataFunc := map[string]interface{}{
						"recipient": recipient,
						"sender":    sender,
						"text":      text2,
					}

					funcRun, ok := b.command[strings.ToLower(text2)]
					if ok {
						go funcRun(dataFunc)
					} else {
						if b.commandElse != nil {
							go b.commandElse(dataFunc)
						}
					}
				}
			}
		}
	} else {
		routeFunc, ok := b.route[r.URL.Path]
		if ok {
			jSonData, err := readJson(r)
			if err != nil {
				return
			}
			routeFunc(jSonData.(map[string]interface{}))
		} else {
			http.FileServer(http.Dir(b.Static)).ServeHTTP(w, r)
		}
	}
}

func (b *Bot) sendToFB(data map[string]interface{}) (string, error) {
	dataJson, err := json.Marshal(data)
	if err != nil {
		return "", errors.New("Error!!!")
	}

	return PostJson(api+b.Token, string(dataJson))
}

func (b *Bot) Route(route string, wfunc workfunc) {
	b.route[route] = wfunc
}

func (b *Bot) Command(command string, wfunc workfunc) {
	b.command[strings.TrimSpace(strings.ToLower(command))] = wfunc
}

func (b *Bot) CommandElse(wfunc workfunc) {
	b.commandElse = wfunc
}

func (b *Bot) Send(data map[string]interface{}) {
	dataSend := make(map[string]interface{})
	dataSend["recipient"] = make(map[string]interface{})
	dataSend["recipient"].(map[string]interface{})["id"] = data["recipient"]

	text, ok := data["text"].(string)
	if ok {
		dataSend["message"] = make(map[string]interface{})
		dataSend["message"].(map[string]interface{})["text"] = text
		b.sendToFB(dataSend)
		return
	}

	title, ok := data["title"].(string)
	subtitle, ok2 := data["subtitle"].(string)
	if ok && ok2 {
		dataSend["message"] = make(map[string]interface{})
		dataSend["message"].(map[string]interface{})["attachment"] = make(map[string]interface{})

		attachment := dataSend["message"].(map[string]interface{})["attachment"].(map[string]interface{})
		attachment["type"] = "template"
		attachment["payload"] = make(map[string]interface{})

		payload := attachment["payload"].(map[string]interface{})
		payload["template_type"] = "generic"
		payload["elements"] = make([]map[string]interface{}, 1)

		dataElement := make(map[string]interface{})
		dataElement["title"] = title
		dataElement["subtitle"] = subtitle

		image_url, ok := data["image_url"]
		if ok {
			dataElement["image_url"] = image_url
		}

		item_url, ok := data["item_url"]
		if ok {
			dataElement["item_url"] = item_url
		}

		payload["elements"].([]map[string]interface{})[0] = dataElement

		b.sendToFB(dataSend)
	}
}

func (b *Bot) Start() {
	mhandler := http.NewServeMux()
	mhandler.HandleFunc("/", b.handler)
	server := &http.Server{
		Addr:    b.Host,
		Handler: mhandler,
	}
	go func() {
		server.ListenAndServe()
	}()

	time.Sleep(time.Second)
	databack, _ := PostJson(apiSub+b.Token, "")
	fmt.Printf("%s\n\n", databack)
}
